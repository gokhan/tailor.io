require "test_helper"

describe SocialConnection do
  let(:social_connection) { SocialConnection.new }

  it "must be valid" do
    value(social_connection).must_be :valid?
  end
end
