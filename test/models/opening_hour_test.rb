require "test_helper"

describe OpeningHour do
  let(:opening_hour) { OpeningHour.new }

  it "must be valid" do
    value(opening_hour).must_be :valid?
  end
end
