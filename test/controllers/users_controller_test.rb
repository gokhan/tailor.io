require "test_helper"

describe UsersController do
  let(:user) { users :one }

  it "gets index" do
    get :index
    value(response).must_be :success?
    value(assigns(:users)).wont_be :nil?
  end

  it "gets new" do
    get :new
    value(response).must_be :success?
  end

  it "creates user" do
    expect {
      post :create, user: { crypted_password: user.crypted_password, email: user.email, salt: user.salt }
    }.must_change "User.count"

    must_redirect_to user_path(assigns(:user))
  end

  it "shows user" do
    get :show, id: user
    value(response).must_be :success?
  end

  it "gets edit" do
    get :edit, id: user
    value(response).must_be :success?
  end

  it "updates user" do
    put :update, id: user, user: { crypted_password: user.crypted_password, email: user.email, salt: user.salt }
    must_redirect_to user_path(assigns(:user))
  end

  it "destroys user" do
    expect {
      delete :destroy, id: user
    }.must_change "User.count", -1

    must_redirect_to users_path
  end
end
