module Site::Cell
  class Map < Trailblazer::Cell
    include Site::Cell::Helpers 

    def addresses
    	AddressRepresenter.for_collection.new(model.collect(&:address).select{|address| address.lat.present? and address.lng.present?}).to_json
    end
  end
end
