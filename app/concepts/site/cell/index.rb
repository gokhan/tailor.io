module Site::Cell
  class Index < Trailblazer::Cell
    include Site::Cell::Helpers 

    def addresses
    	AddressRepresenter.for_collection.new(Address.all.select{|address| address.lat.present? and address.lng.present?}).to_json
    end
  end
end
