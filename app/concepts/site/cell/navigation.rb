module Site::Cell
  class Navigation < Trailblazer::Cell
    include ActionView::Helpers::CsrfHelper

    def current_user
    	context[:current_user]
    end

    def has_tailor?
    	current_user and current_user.tailor.present?
    end

    def dashboard_url
      if current_user.tailor.present?
	  	  "/dashboard/tailors/#{current_user.tailor.slug}" 
      else
        '/'
      end
	 end

    # Admin links
    def tailors_url
      "/dashboard/tailors"
    end    

    def pages_url
      "/dashboard/pages"
    end    

    def users_url
      "/dashboard/users"
    end   
        
  end
end