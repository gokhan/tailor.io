module Site::Cell
  class Dashboard < Trailblazer::Cell
    include ActionView::Helpers::CsrfHelper
    def tailor
      Tailor.friendly.find_by(slug: params[:tailor_id] || params[:id])
    end

    # Admin links
    def tailors_url
      "/dashboard/tailors"
    end    

    def pages_url
      "/dashboard/pages"
    end    

    def users_url
      "/dashboard/users"
    end  
  end
end