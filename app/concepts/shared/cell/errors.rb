module Shared::Cell
  class Errors < Trailblazer::Cell
    include ::Cell::Slim

    def errors
      model.errors.messages
    end
	end
end
