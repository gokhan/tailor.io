module Shared::Cell
	module Helpers
		def self.included(base)
			base.send :include, ActionView::Helpers::DateHelper
			base.send :include, ActionView::Helpers::TextHelper
			base.send :include, ActionView::Helpers::FormOptionsHelper
			base.send :include, ActionView::Helpers::FormHelper
			base.send :include, ActionView::RecordIdentifier
			base.send :include, MoneyRails::ActionViewExtension
		end

		def errors
			options["errors"]
		end
	end
end
