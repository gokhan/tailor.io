module Page::Contract
	class Create < Reform::Form
		property :slug
		property :name
		property :content
		
    validation :default do
      required(:slug).filled
      required(:name).filled
      required(:content).filled
    end      
	end
end