module Page::Cell
  class Show < Trailblazer::Cell
    property :slug
    property :name
    property :content
  end
end
