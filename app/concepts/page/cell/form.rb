module Page::Cell
  class Form < Trailblazer::Cell
    include Site::Cell::Helpers
	
		property :slug    
    property :name
    property :content
  end
end
