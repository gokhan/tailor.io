class Page::Policy < ApplicationPolicy
	def update?
		admin?
	end
end