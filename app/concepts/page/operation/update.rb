class Page::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	:model!
		step  Policy::Pundit( Page::Policy, :update? )								
	  step  Contract::Build(constant: Page::Contract::Create)

	  def model!(options, params:, **)
	  	options["model"] = Page.friendly.find(params[:id])
	  end
	end

	step  Nested( Present )
  step  Contract::Validate(key: "page")
  step  Contract::Persist()
end