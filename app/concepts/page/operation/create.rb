class Page::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( Page, :new )
		step  Policy::Pundit( Page::Policy, :update? )						
	  step  Contract::Build(constant: Page::Contract::Create)
	end

  step  	Nested( Present )
  step  	Contract::Validate(key: "page")
  failure Contract::Persist(method: :sync)
  step 		Contract::Persist()
end