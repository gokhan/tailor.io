class Page::Show < Trailblazer::Operation
  step  :model!

	def model!(options, params:, **)
		options["model"] = Page.find_by(slug: params[:id])
	end
end