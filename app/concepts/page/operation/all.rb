class Page::All < Trailblazer::Operation
	step  :model!
	step  Policy::Pundit( Page::Policy, :update? )				

	def model!(options, params:, **)
		options["model"] = Page.all
	end
end