module Address::Cell
  class Form < Trailblazer::Cell
    include Site::Cell::Helpers

		def tailor
			model.model.addressable
		end    
  end
end
