class Address::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( Address, :new)
		step  Policy::Pundit( Address::Policy, :update? )		
		step  :assing_tailor!
		step  Contract::Build(constant: Address::Contract::Create)
		def assing_tailor!(options, params:, **)
	    options["model"].addressable = Tailor.friendly.find(params[:tailor_id])
	  end
	end	
	
	step  Nested( Present )
  step  Contract::Validate(key: "address")
  step  Contract::Persist()
end