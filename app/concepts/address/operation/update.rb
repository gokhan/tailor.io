class Address::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	:model!
		step  Policy::Pundit( Address::Policy, :update? )				
	  step  Contract::Build(constant: Address::Contract::Create)
	  def model!(options, params:, **)
	    options["model"] = Tailor.friendly.find(params[:tailor_id]).address
	  end
	end	
	
  step  Nested( Present )
  step  Contract::Validate(key: "address")
  step  Contract::Persist()
end