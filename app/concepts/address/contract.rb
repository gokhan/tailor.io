module Address::Contract
	class Create < Reform::Form
		property :address
		property :house_number
		property :street      
		property :city         
		property :postcode     
		property :state        
		property :country      
		property :lat          
		property :lng          

		property :phone
		property :email

    validation :default do
      required(:address).filled
    end
	end
end