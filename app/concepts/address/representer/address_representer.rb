require 'representable/json'

class AddressRepresenter < Representable::Decorator
  include Representable::JSON

  property :lat
  property :lng
  property :info, exec_context: :decorator
  property :addressable do
  	property :slug
  	property :name
  end

  def info
    model = self.represented
    tailor  = model.addressable
    result = []
    result << "<a href='/tailors/#{tailor.slug}'><strong>#{tailor.name}</strong></a>"
    if model
        result << "#{model.street} #{model.house_number}" 
        result << "#{model.postcode} #{model.city}"
    end
    result.join("<br/>")
  end
  collection_representer class: Address
end

#  id               :integer          not null, primary key
#  address          :string
#  addressable_id   :integer
#  addressable_type :string
#  house_number     :string
#  street           :string
#  city             :string
#  postcode         :string
#  state            :string
#  country          :string
#  lat              :decimal(10, 6)
#  lng              :decimal(10, 6)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  phone            :string
#  email            :string


# Table name: tailors
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  slug        :string
