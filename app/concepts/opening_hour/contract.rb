module OpeningHour::Contract
	class Create < Reform::Form
		property :content

    validation :default do
      required(:content).filled
    end		
	end
end