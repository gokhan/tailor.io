module OpeningHour::Cell
  class Edit < Trailblazer::Cell
    include Site::Cell::Helpers

		def tailor
			model.model.tailor
		end    
  end
end
