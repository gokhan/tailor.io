class OpeningHour::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	:model!
		step  Policy::Pundit( Address::Policy, :update? )				
	  step  Contract::Build(constant: OpeningHour::Contract::Create)
	  def model!(options, params:, **)
	    options["model"] = Tailor.friendly.find(params[:tailor_id]).opening_hour 
	  end
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "opening_hour")
  step  Contract::Persist()
end