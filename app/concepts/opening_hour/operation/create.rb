class OpeningHour::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( OpeningHour, :new )
		step  Policy::Pundit( Address::Policy, :update? )				
		step  :assign_tailor!
	  step  Contract::Build(constant: OpeningHour::Contract::Create)
	  def assign_tailor!(options, params:, **)
	    options["model"].tailor = Tailor.friendly.find(params[:tailor_id])
	  end
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "opening_hour")
  failure Contract::Persist(method: :sync)
  step  Contract::Persist()
end