class Image < ActiveRecord::Base
  class Destroy < Trailblazer::Operation
    step  Model( Image, :find_by )
    step  :process!

    def process!(options, params: , **)
      options["model"].destroy!
    end
  end
end