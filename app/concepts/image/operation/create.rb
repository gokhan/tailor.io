class Image < ActiveRecord::Base
  class Create < Trailblazer::Operation
    extend Contract::DSL
      
    contract do
      property :data
      property :imageable
      property :tag_list
  
      validation :default do
        required(:data).filled
        required(:imageable).filled
      end      
    end

    step Model( Image, :new )
    step :assign_imageable!
    step Contract::Build()
    step Contract::Validate()
    step Contract::Persist()

    def assign_imageable!(options, params:, **)
      options["model"].imageable = Tailor.friendly.find( params.fetch(:tailor_id) )
    end 
  end
end