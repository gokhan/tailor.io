class Image < ActiveRecord::Base
  class Logolize < Trailblazer::Operation
    step  Model( Image, :find_by )
    step  :process!

    def process!(options, params: , **)
      options["model"].imageable.images.map{ |image| 
        image.tag_list.remove('logo')
        image.save
      }
      options["model"].tag_list = 'logo'
      options["model"].save
      options["model"].move_to_top
    end
  end
end