class Image < ActiveRecord::Base
  class Star < Trailblazer::Operation
    step  Model( Image, :find_by )
    step  :process!

    def process!(options, params: , **)
      options["model"].move_to_top
    end
  end
end