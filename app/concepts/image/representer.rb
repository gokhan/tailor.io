require 'representable/json'

class ImageRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :tag_list

  property :data do
    property :thumb
    property :square
  end
end