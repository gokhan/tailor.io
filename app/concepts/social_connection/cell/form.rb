module SocialConnection::Cell
  class Form < Trailblazer::Cell
    include Site::Cell::Helpers

		def tailor
			model.tailor
		end    
  end
end
