module SocialConnection::Contract
	class Create < Reform::Form
		property :tailor 
		property :facebook
		property :twitter
		property :youtube
		property :instagram
		property :googleplus

    validation :default do
      required(:tailor).filled
    end		
	end
end
