class SocialConnection::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( SocialConnection, :new )
		step  :assign_tailor!
		step  Policy::Pundit( SocialConnection::Policy, :update? )												

	  step  Contract::Build( constant: SocialConnection::Contract::Create )
	  def assign_tailor!(options, params:, **)
	    options["model"].tailor = Tailor.friendly.find(params[:tailor_id])
	  end
	end	
	
  step  Nested( Present )
  step  Contract::Validate(key: "social_connection")
  failure Contract::Persist(method: :sync)
  step  Contract::Persist()
end