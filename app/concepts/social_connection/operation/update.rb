class SocialConnection::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	:model!
		step  Policy::Pundit( SocialConnection::Policy, :update? )														
	  step  Contract::Build( constant: SocialConnection::Contract::Create )
	  def model!(options, params:, **)
	    options["model"] = Tailor.friendly.find(params[:tailor_id]).social_connection 
	  end
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "social_connection")
  step  Contract::Persist()
end