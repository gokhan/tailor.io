class Email::NewUser < Trailblazer::Operation
	step  :process!

	def process!(options, params:, **)		
		mailgun = Mailgun::Client.new ENV['MAILGUN_API_KEY']
		parameters = {
		  :to => "gokhan@sylow.net",
		  :subject => "New user registered: #{params[:user].email}",
		  :text => "yeah, we're gonna need you to come in on friday...yeah.",
		  :from => "noreply@tailorrepair.nl"
		}
		result = mailgun.send_message( ENV['MAILGUN_DOMAIN'], parameters )
		puts result.to_h.inspect
	end
end