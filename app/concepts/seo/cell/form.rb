module Seo::Cell
  class Form < Trailblazer::Cell
    include Site::Cell::Helpers

		def tailor
			model.model.tailor
		end    
  end
end
