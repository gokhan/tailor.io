class Seo::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	:model!
		step  Policy::Pundit( Seo::Policy, :update? )														
	  step  Contract::Build( constant: Seo::Contract::Create )
	  def model!(options, params:, **)
	    options["model"] = Tailor.friendly.find(params[:tailor_id]).seo 
	  end
	end	
	
  step  Nested( Present )
  step  Contract::Validate(key: "seo")
  step  Contract::Persist()
end