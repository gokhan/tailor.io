class Seo::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( Seo, :new )
		step  :assign_tailor!
		step  Policy::Pundit( Seo::Policy, :update? )												
	  step  Contract::Build(constant: Seo::Contract::Create)
	  def assign_tailor!(options, params:, **)
	    options["model"].tailor = Tailor.friendly.find(params[:tailor_id])
	  end
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "seo")
  failure Contract::Persist(method: :sync)
  step  Contract::Persist()

  def assign_tailor!(options, params:, **)
    options["model"].tailor = Tailor.friendly.find(params[:tailor_id])
  end
end