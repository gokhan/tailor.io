class Seo::Policy < ApplicationPolicy
	def update?
		admin? or owner?
	end

	private
	def owner?
		model.tailor.user == user
	end
end