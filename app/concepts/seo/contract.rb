module Seo::Contract
	class Create < Reform::Form
		property :title
		property :keywords 
		property :description
    validation :default do
      required(:title).filled
      required(:description).filled
      required(:keywords).filled
    end		
	end
end