class User::All < Trailblazer::Operation
	step  :model!	
	step  Policy::Pundit( User::Policy, :admin? )															

	def model!(options, params:, **)
		options["model"] = User.all
	end
end