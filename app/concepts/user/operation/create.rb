class User::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( User, :new )
    step  Policy::Pundit( User::Policy, :create? )                             
	  step  Contract::Build( constant: User::Contract::Create )
	end

  step  	Nested( Present )
  step  	Contract::Validate(key: "user")
  failure Contract::Persist(method: :sync)
  step 		:save!
  step    :send_notification!
  def save!(options, params:, **)
  	options["contract.default"].save
  end

  def send_notification!(options)
    Email::NewUser.(user: options["model"])
  end
end