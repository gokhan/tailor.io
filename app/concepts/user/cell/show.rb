module User::Cell
  class Show < Trailblazer::Cell
    property :name
    property :description
    property :user
    property :images
    property :slug

    def image_post_url
        "/dashboard/tailors/#{model.slug}/images"
    end
  end
end
