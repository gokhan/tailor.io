module User::Contract
	class Create < Reform::Form
    EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
		
		property :name
		property :email
		property :password
		property :password_confirmation, virtual: true

    validation :default do
      configure do
        option :form
        config.messages_file = 'config/locales/error_messages.yml'
        def email?(value)          
          value =~ EMAIL_REGEX
        end

        def unique?(value)
          User.where.not(id: form.model.id).find_by(email: value).nil?
        end
      end

      required(:name).filled
      required(:email).filled(:email?, :unique?)
      required(:password).filled(min_size?: 6).confirmation      
    end
	end
end