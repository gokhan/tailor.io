module Tailor::Contract
	class Create < Reform::Form
		property :name
		property :description
		property :tag_line

    validation :default do
      required(:name).filled
      required(:tag_line).filled(min_size?: 10)
    end
	end
end