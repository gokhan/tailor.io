class Tailor::Url::Seo < Trailblazer::Operation
  step :model!

  def model!(options, params:, **)
    tailor = params[:tailor] 
    if tailor.seo
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/seos/#{tailor.seo.id}/edit")
    else
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/seos/new")
    end
  end

end