class Tailor::Url::SocialConnection < Trailblazer::Operation
  step :model!

  def model!(options, params:, **)
    tailor = params[:tailor] 
    if tailor.social_connection
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/social_connections/#{tailor.social_connection.id}/edit")
    else
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/social_connections/new")
    end
  end

end