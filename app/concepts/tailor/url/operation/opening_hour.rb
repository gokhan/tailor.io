class Tailor::Url::OpeningHour < Trailblazer::Operation
  step :model!

  def model!(options, params:, **)
    tailor = params[:tailor] 
    if tailor.opening_hour
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/opening_hours/#{tailor.opening_hour.id}/edit")
    else
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/opening_hours/new")
    end
  end

end