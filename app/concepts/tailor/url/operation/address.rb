class Tailor::Url::Address < Trailblazer::Operation
  step :model!

  def model!(options, params:, **)
    tailor = params[:tailor] 
    if tailor.address
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/addresses/#{tailor.address.id}/edit")
    else
    	options["model"] = OpenStruct.new(url: "/dashboard/tailors/#{tailor.slug}/addresses/new")
    end
  end
end