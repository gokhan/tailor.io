module Tailor::Cell
  class Images < Trailblazer::Cell
    protected
    def image_post_url
        "/dashboard/tailors/#{model.slug}/images"
    end    
  end
end
