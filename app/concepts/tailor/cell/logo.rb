module Tailor::Cell
  class Logo < Trailblazer::Cell
    protected
    property :logo
  end
end
