module Tailor::Cell
  class Index < Trailblazer::Cell
    include Site::Cell::Helpers

    def owner_name(tailor)
    	if tailor.user
            "%s -%s" % [tailor.user.name, tailor.user.email]
    	else
    		'--'
    	end
    end
  end
end
