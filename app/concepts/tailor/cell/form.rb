module Tailor::Cell
  class Form < Trailblazer::Cell
    include Site::Cell::Helpers
    
    property :name
    property :description
    property :user
    property :images
  end
end
