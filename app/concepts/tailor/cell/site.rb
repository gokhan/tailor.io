module Tailor::Cell
  class Site < Trailblazer::Cell
    include ::Site::Cell::Helpers

    property :name
    property :description
    property :user
    property :images
    property :slug
    property :avatar
    property :address
    property :opening_hour
    property :social_connection
    property :logo
    property :prices
    
    def main_image
      images.tagged_with('carousel').first or Image.first
    end
    def preview_prices
      prices.where(preview: true)
    end
    def map_info_content
        result = []
        result << "<strong>#{name}</strong>"
        if address
            result << "#{address.street} #{address.house_number}" 
            result << "#{address.postcode} #{address.city}"
        end
        result.join("<br/>")
    end
  end
end