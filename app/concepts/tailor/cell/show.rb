module Tailor::Cell
  class Show < Trailblazer::Cell
    include Site::Cell::Helpers    
    property :name
    property :description
    property :user
    property :images
    property :slug

    def tailor
        TailorRepresenter.new(model).as_json
    end

    def image_post_url
        "/dashboard/tailors/#{model.slug}/images"
    end
  end
end
