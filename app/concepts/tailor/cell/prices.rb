module Tailor::Cell
  class Prices < Trailblazer::Cell
    include Site::Cell::Helpers
    
    property :name
    property :prices

    def price_with_categories
    	Price.joins(price_list: :price_category).where("prices.tailor_id=%s", model.id).select("prices.*, price_lists.price_category_id as price_category_id")
    end

    def consumer_price(price)
    	humanized_money_with_symbol price.price_consumer
    end

    def business_price(price)
    	if price.price_business > 0
    		humanized_money_with_symbol price.price_business
    	else
    		'-'
    	end
    end
  end
end