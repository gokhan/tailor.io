module Tailor::Cell
  class Header < Trailblazer::Cell
    include Site::Cell::Helpers
    
    property :name
    property :description
    property :user
    property :images
  end
end
