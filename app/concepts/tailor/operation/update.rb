class Tailor::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	:model!
		step  Policy::Pundit( Tailor::Policy, :update? )
	  step  Contract::Build(constant: Tailor::Contract::Create)
	  def model!(options, params:, **)
	    options["model"] = Tailor.friendly.find(params[:id])
	  end
	end

	step  Nested( Present )
  step  Contract::Validate(key: "tailor")
  step  Contract::Persist()
end