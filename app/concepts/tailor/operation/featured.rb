class Tailor::Featured < Trailblazer::Operation
  step :model!

  def model!(options, **)
    options["model"] = Tailor.all
  end

end