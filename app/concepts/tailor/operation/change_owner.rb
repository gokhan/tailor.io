class Tailor::ChangeOwner < Trailblazer::Operation
	class Present < Trailblazer::Operation
	  extend Contract::DSL
	  contract do
	    property :user_id
	  end

		step	:model!
		step  Policy::Pundit( Tailor::Policy, :create? )		
	  step  Contract::Build( )
	  def model!(options, params:, **)
	  	options["model"] = Tailor.friendly.find_by(slug: params[:id])
	  end
	end

  step  	Nested( Present )
  step  	Contract::Validate(key: "tailor")
  step 		Contract::Persist()
end