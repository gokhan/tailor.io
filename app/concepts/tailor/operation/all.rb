class Tailor::All < Trailblazer::Operation
	step  :model!
	step  Policy::Pundit( Tailor::Policy, :admin? )
	
	def model!(options, params:, **)
		options["model"] = Tailor.order(:name)
	end
end