class Tailor::Search < Trailblazer::Operation
	step  :model!
	
	def model!(options, params:, **)
		addresses =   if params[:lat].present? and params[:lng].present? 
									  Address.near([params[:lat], params[:lng]], 50)
									else
										Address.near(params[:city], 50)
									end			
		options["model"] =	addresses.collect(&:addressable)
	end
end