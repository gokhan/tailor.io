class Tailor::Show < Trailblazer::Operation
  step :model!

  def model!(options, params:, **)
    options["model"] = Tailor.friendly.find(params[:id]) 
  end
end