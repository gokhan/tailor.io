class Tailor::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( Tailor, :new )
		step  Policy::Pundit( Tailor::Policy, :create? )
		step  :assign_user!
	  step  Contract::Build(constant: Tailor::Contract::Create)
		def assign_user!(options, params:, **)
	    options["model"].user = options["current_user"] 
	  end
	end

  step  	Nested( Present )
  step  	Contract::Validate(key: "tailor")
  failure Contract::Persist(method: :sync)
  step 		Contract::Persist()
end