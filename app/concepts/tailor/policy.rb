class Tailor::Policy < ApplicationPolicy
	def create?
		admin?
	end

	def update?
		admin? or owner?
	end
end