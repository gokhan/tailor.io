require 'representable/json'

class TailorRepresenter < Representable::Decorator
  include Representable::JSON
  property :logo, exec_context: :decorator

  property :logo do
  	property :data do
	    property :thumb
	    property :square
	    property :medium_square
	  end
	end  	

  collection :images do
  	property :id
  	property :data do
	    property :thumb
	    property :square
	    property :medium_square
	  end
	  property :tag_list
  end


  def logo
  	represented.logo
  end
end