class Price::Policy < ApplicationPolicy
	def update?
		admin? or owner?
	end

	def all?
		return true if admin?
		user.tailor.prices == record
	end

	private
	def owner?
		model.tailor.user == user
	end
end