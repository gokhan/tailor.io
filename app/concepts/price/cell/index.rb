module Price::Cell
  class Index < Trailblazer::Cell
    include Site::Cell::Helpers

    def price_list(price)
    	price.price_list ? price.price_list.name : 'none'
    end

    def category(price)
    	price.price_list.price_category ? price.price_list.price_category.name : 'none' 
    end
    
    def yes_or_no(value)
    	value ? 'yes' : 'no'
    end
  end
end
