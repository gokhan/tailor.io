module Price::Contract
	class Create < Reform::Form
		property :tailor 
		property :price_consumer
		property :price_business
		property :price_list_id
		property :preview

    validation :default do
      required(:tailor).filled
      required(:price_list_id).filled
      required(:price_consumer).filled
    end		
	end
end
