class Price::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	Model( Price, :find_by )
		step  Policy::Pundit( Price::Policy, :update? )												
	  step  Contract::Build( constant: Price::Contract::Create )
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "price")
  step  Contract::Persist()
end