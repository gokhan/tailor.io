class Price::Delete < Trailblazer::Operation
	step Model( Price, :find_by )
	step  Policy::Pundit( Price::Policy, :update? )											
	step :destroy!

	def destroy!(options, **)
		options["model"].destroy
	end
end