class Price::Index < Trailblazer::Operation
	step :model!
	step  Policy::Pundit( Price::Policy, :all? )

	def model!(options, params:, **)
		tailor = Tailor.friendly.find_by(slug: params[:tailor_id])
		options["model"] = tailor.prices.joins(price_list: :price_category).order("price_categories.name")
	end
end