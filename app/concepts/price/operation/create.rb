class Price::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
		step	:model!
		step  Policy::Pundit( Price::Policy, :update? )										
		step  :assign_tailor!
	  step  Contract::Build( constant: Price::Contract::Create )
	  def assign_tailor!(options, params:, **)
	    options["model"].tailor = Tailor.friendly.find(params[:tailor_id])
	  end

	  def model!(options, params:, **)
	  	tailor = Tailor.friendly.find(params[:tailor_id])
	  	options["model"] = tailor.prices.find_by(price_list_id: params.fetch(:price, {}).fetch(:price_list_id, nil) ) || tailor.prices.build
	  end
	end	
	
  step  Nested( Present )
  step  Contract::Validate(key: "price")
  failure Contract::Persist(method: :sync)
  step  Contract::Persist()
end