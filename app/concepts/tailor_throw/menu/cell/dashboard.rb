module Tailor::Cell::Menu
  class Dashboard < Trailblazer::Cell
    include ActionView::Helpers::CsrfHelper

    def images_edit_url
      "/dashboard/tailors/#{tailor.slug}/images"
    end

    def tailor_edit_url
    	"/dashboard/tailors/#{tailor.slug}/edit"
    end

    def tailor_url
    	"/dashboard/tailors/#{tailor.slug}"
    end

    # Admin links
    def tailors_url
      "/dashboard/tailors"
    end    

    def pages_url
      "/dashboard/pages"
    end    

    def users_url
      "/dashboard/users"
    end   
    
    def prices_url
      "/dashboard/tailors/#{tailor.slug}/prices"
    end   
    def address_edit_url
      result = Tailor::Url::Address.(tailor: tailor)
      result["model"].url     
    end

    def seo_edit_url
      result = Tailor::Url::Seo.(tailor: tailor)
      result["model"].url     
    end

    def social_connection_url
      result = Tailor::Url::SocialConnection.(tailor: tailor)
      result["model"].url     
    end

    def opening_hour_url
      result = Tailor::Url::OpeningHour.(tailor: tailor)
      result["model"].url     
    end

    def admin_url
      "/dashboard/tailors"
    end

    def admin?
      true
    end

    def tailor
      Tailor.friendly.find_by(slug: params[:tailor_id] || params[:id])
    end
  end
end