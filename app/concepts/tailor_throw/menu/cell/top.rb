module Tailor::Cell::Menu
  class Top < Trailblazer::Cell
    include ActionView::Helpers::CsrfHelper
    def tailors_url
      "/dashboard/tailors"
    end    
    def pages_url
      "/dashboard/pages"
    end    
  end
end