module Tailor::Cell
  class Footer < Trailblazer::Cell
    include ActionView::Helpers::CsrfHelper

    def pages
    	Page.footer
    end    
  end
end