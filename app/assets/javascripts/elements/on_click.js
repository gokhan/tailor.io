$(document).ready(function(){
	$('a[data-type="image.delete"]').click(function(){
		var me = this;
		$.post('/dashboard/tailors/terzi-amersfoort/images/' + $(me).data('id'), { "_method":"delete" } )
					.done(function(){
				    var element= $(me).closest('.column');
    				element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){this.remove()});
    				element.addClass('animated flipOutX');
					});
	}); 

	$('a[data-type="image.logolize"]').click(function(){
		var me = this;
		$.post('/dashboard/tailors/terzi-amersfoort/images/' + $(me).data('id') + '/logolize')
					.done(function(data){
						console.log(data);
				    var element= $(me).closest('.column');
    				element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){this.remove()});
    				element.addClass('animated flipOutX'); 
					});
	});	
});
