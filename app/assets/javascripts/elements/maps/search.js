function initSearchMap() {
  if ( $('#searchmap').length == 0 ) return;

  createSearchMap();
}
// Map on Search page
function createSearchMap() {
  map = new google.maps.Map(document.getElementById('searchmap'), {
    zoom: 12
  });
}


function addMarkers(json){
  var iconBase = '/mapicons/';
  var icons = {
    tailor: {
      icon: iconBase + 'tailor.png'
    },
  };

  for (var i = 0; i < json.length; i++) {
    var marker;
    addMarkerWithTimeout(json[i]);
    continue;
  }
}


function addMarkerWithTimeout(address) {
  window.setTimeout(function() {
    var marker = new google.maps.Marker({
      position: {lat: +address.lat, lng: +address.lng},
      map: map,
      icon: '/mapicons/tailor.png',
      infoWindow: {
        content: address.info
      },
      animation: google.maps.Animation.DROP
    });

    var infowindow = new google.maps.InfoWindow({
      content: address.info,
    });
    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
  }, 300);
}


function setCenter(first){
  var coord;
  if (first == undefined)
    coord = {lat:52.660304,lng: 4.768325}
  else{
    coord = {lat: +first.lat, lng: +first.lng };
  }
  map.setCenter(coord);
}
