function initTailorMap() {
  if ( $('#tailormap').length == 0 ) return;

  createTailorMap();
}
// Map on Search page
function createTailorMap() {
  map = new google.maps.Map(document.getElementById('tailormap'), {
    zoom: 14
  });
}

function addMarker(coord){
  var iconBase = '/mapicons/';
  var icons = {
    tailor: {
      icon: iconBase + 'tailor.png'
    },
  };
  new google.maps.Marker({
      position: {lat: +coord.lat, lng: +coord.lng},
      map: map,
      icon: icons.tailor.icon
  });
  map.setCenter(coord);
}
