function initAddressForm() {
  if( $('#shop_map').length == 0 ) return;
  createAddressMap();

  if( $('#address_lat').length == 0 ) return;
  coord = {lat: parseFloat($('#address_lat').val()), lng: parseFloat($('#address_lng').val()) };
  setCenter(coord);
}

function createAddressMap(){
  map = new google.maps.Map(document.getElementById('shop_map'), {
    zoom: 14,
  });
}

var autofill;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

var address_mapping = {
  street_number: 'address_house_number',
  route: 'address_street',
  locality: 'address_city',
  administrative_area_level_1: 'address_state',
  country: 'address_country',
  postal_code: 'address_postcode'
}


function initAutoComplete() {
  if ( $('#autoaddress').length ){
    var options = {
      types: ['geocode'],
      componentRestrictions: {country: "nl"}
    };
    var inputAddress = document.getElementById('autoaddress');
    autofill = new google.maps.places.Autocomplete(inputAddress, options);
    autofill.addListener('place_changed', fillInAddress);  
  }
}

function fillInAddress() {
  // Get the place details from the autocomplete object
  var place = autofill.getPlace();
  if(place.geometry.location.lat()){
    document.getElementById('address_lat').value = place.geometry.location.lat();
    document.getElementById('address_lng').value = place.geometry.location.lng();
    initMap();
  }


  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(address_mapping[addressType]).value = val;
    }
  }
}