var autocomplete;

function initCitiesAutoComplete() {
  if ( $('#autocomplete').length == 0 ) return ;
  var options = {
    types: ['(cities)'],
    componentRestrictions: {country: "nl"}
  };
  var input = document.getElementById('autocomplete');
  autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.addListener('place_changed', redirectToSearch);  
}

function redirectToSearch() {
  var place = autocomplete.getPlace();
  if(place.geometry.location.lat()){
    location.assign( '/home/search?lat=' + place.geometry.location.lat() + '&lng=' + place.geometry.location.lng() );
  }  
}