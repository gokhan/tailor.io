$(function() {
  $(".button-dropzone").each(function(){
    var btn = this;
    new Dropzone(this, {
      url: 'change-me-in-processing',
      paramName: 'data',
      headers: {'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')},
      processing: function(){
        $.fancybox.open({src:'#loader', type:'inline', closeBtn:false})
        this.options.url = $(".button-dropzone").data('post-url');
      },
      success: function(a, response){
        //document.getElementById('images').innerHTML =  response.data;
        //Use on complete event handler
        reactGlobal.injectMethod(response.images)
        $.fancybox.close( 'all' );
      },
      addedfile: function(file){ 
        //keep this method so it does append template html into page
      },
      sending: function(file, xhr, formData){
        formData.append('tag_list', $(btn).data('tag-list'));
      },
    })
  })
});
