var map;
function initMap() {
	initCitiesAutoComplete();
  initAutoComplete();
  initAddressForm();
  initSearchMap();
  initTailorMap();
}