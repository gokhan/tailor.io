$(function() {
    var editor = new MediumEditor('.editable', {
        toolbar: {
            /* These are the default options for the toolbar,
               if nothing is passed this is what is used */
            allowMultiParagraphSelection: true,
            buttons: ['bold', 'italic', 'underline', 'anchor', 
            {
                name: 'h1',
                classList: ['title']
            }, 'h2', 'h3', 'quote'],
            diffLeft: 0,
            diffTop: -10,
            firstButtonClass: 'medium-editor-button-first',
            lastButtonClass: 'medium-editor-button-last',
            relativeContainer: null,
            standardizeSelectionStart: false,
            static: false,
            /* options which only apply when static is true */
            align: 'center',
            sticky: false,
            updateOnEmptySelection: false
        }
    });

    $('.editable').bind('input propertychange', function() {
        console.log($(this).html());
      $("#" + $(this).data("field-id")).val($(this).html());
    });    
});



        //     {
        //         name: 'h1',
        //         action: 'append-h2',
        //         aria: 'header type 1',
        //         tagNames: ['h2'],
        //         contentDefault: '<b>H1</b>',
        //         classList: ['title']
        //     },
        // , 