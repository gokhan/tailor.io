var reactGlobal = {injectMethod:function(){}};
class Images extends React.Component {

  constructor(props) {
      super(props);
      this.state =  {images: this.props.images, logo: this.props.logo};
  }
  
  logolize(new_logo) {
    this.setState({logo: new_logo});
  }
  
  componentWillMount(){
    var me = this;
    reactGlobal.injectMethod = function(data) {
      me.setState( {images: data} );
    };
  }

  render() {
    me = this
    images = this.state.images.map(function(image){
      return (
       <SingleImage image={image} key={image.id} logolize={me.logolize.bind(me)} tailor={me.props.tailor}/>
      );
    });
    return(
            <div className="tile is-ancestor">
              <div className="tile is-parent">
                <div className="tile is-child box"> 
                  {this.logo()}        
                </div>
              </div>
              <div className="tile is-8 is-vertical is-parent">
                <div className="tile is-child box">
                  <div className='columns is-multiline'>
                    {this.uploader()}
                    {images}
                  </div>
                </div>
              </div>
            </div>  
          )
  }

  logo() {
    return(
            <div className='card' key={this.state.logo.id}>
              <header className="card-header">
                <p className="card-header-title">
                  Your Logo
                </p>        
              </header>
              <div className='card-image'>
                <figure className='image is-square'>
                  <img src={this.state.logo.data.medium_square.url} />
                </figure>
              </div>
            </div>      
          )
  }

  uploader() {
    post_url = '/dashboard/tailors/'+ this.props.tailor + '/images'
    return(
      <div className='column is-3'>
        <div className='class'>
          <figure className='image is-square'>
            <img src='/images/add-image.png' className='button-dropzone' data-post-url={post_url} data-tag-list='carousel' data-type='carousel' />
          </figure>
        </div>
      </div>
    )
  }
}
