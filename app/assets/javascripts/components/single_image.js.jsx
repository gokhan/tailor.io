class SingleImage extends React.Component {
	constructor(props) {
      super(props);
      if (this.props){
        this.state =  this.props.image;
      }
  }

  render() {
    return (
      <div className='column is-3' ref='image'>
        <div className='card'>
          <div className='card-image'>
            <figure className='image is-square'>
              <img src={this.state.data.medium_square.url} />
            </figure>
          </div>
          <footer className='card-footer'>
            <a className='card-footer-item' title='Make it logo' onClick={this.logolize.bind(this)}>
              <div className='icon is-big'>
                <i className='fa fa-address-book' />
              </div>
            </a>
            <a className='card-footer-item' title='Make it logo' onClick={this.delete.bind(this)} data-confirrm='Are you sure to delete?'>
              <div className='icon is-big'>
                <i className='fa fa-trash' />
              </div>
            </a>
          </footer>
        </div>
      </div>
    );
  }

  delete() {
		var element= $(this.refs.image);
    $.post('/dashboard/tailors/' + this.props.tailor + '/images/' + this.state.id, { "_method":"delete" } )
          .done(function(){
            element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){this.remove()});
            element.addClass('animated flipOutX');
          });
  } 

  logolize() {
  	var me = this;
    $.post('/dashboard/tailors/' + this.props.tailor + '/images/' + this.state.id + '/logolize')
          .done(function(){
            me.props.logolize(me.state);
          });
  }
}

