# == Schema Information
#
# Table name: price_lists
#
#  id                :integer          not null, primary key
#  name              :string
#  price_category_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_price_lists_on_price_category_id  (price_category_id)
#

class PriceList < ActiveRecord::Base
  belongs_to :price_category
  has_many :prices
end
