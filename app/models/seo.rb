# == Schema Information
#
# Table name: seos
#
#  id          :integer          not null, primary key
#  tailor_id   :integer
#  title       :string
#  description :string
#  keywords    :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_seos_on_tailor_id  (tailor_id)
#

class Seo < ActiveRecord::Base
  belongs_to :tailor
end
