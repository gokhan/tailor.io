# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  slug       :string
#  name       :string
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  in_footer  :boolean          default(FALSE)
#  position   :integer
#

class Page < ActiveRecord::Base
	scope :footer, -> { where(in_footer: true).order(:position) }
	acts_as_list

	extend FriendlyId
  friendly_id :name, use: :slugged
end
