# == Schema Information
#
# Table name: addresses
#
#  id               :integer          not null, primary key
#  address          :string
#  addressable_id   :integer
#  addressable_type :string
#  house_number     :string
#  street           :string
#  city             :string
#  postcode         :string
#  state            :string
#  country          :string
#  lat              :decimal(10, 6)
#  lng              :decimal(10, 6)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  phone            :string
#  email            :string
#  uri              :string
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#

class Address < ActiveRecord::Base
	geocoded_by :address, latitude: :lat, longitude: :lng
  belongs_to :addressable, polymorphic: true
end
