# == Schema Information
#
# Table name: users
#
#  id               :integer          not null, primary key
#  name             :string
#  email            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  crypted_password :string
#  salt             :string
#  admin            :boolean          default(FALSE)
#

class User < ActiveRecord::Base
  has_one :tailor, dependent: :nullify

  attr_accessor :email, :password, :password_confirmation, :authentications_attributes
  authenticates_with_sorcery! do |config|
    config.authentications_class = Authentication
  end

  has_many :authentications, :dependent => :destroy
  accepts_nested_attributes_for :authentications
end
