# == Schema Information
#
# Table name: price_categories
#
#  id           :integer          not null, primary key
#  name         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  key          :string
#  dry_cleaning :boolean          default(FALSE)
#

class PriceCategory < ActiveRecord::Base
  has_many :price_lists
  has_many :prices, through: :price_lists

  def title
  	if dry_cleaning
  		"#{name} (stomerij)"
  	else
  		name
  	end
  end
end
