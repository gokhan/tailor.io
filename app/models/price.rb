# == Schema Information
#
# Table name: prices
#
#  id                   :integer          not null, primary key
#  tailor_id            :integer
#  price_consumer_cents :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  preview              :boolean          default(FALSE)
#  price_business_cents :integer
#  price_list_id        :integer
#
# Indexes
#
#  index_prices_on_price_list_id  (price_list_id)
#  index_prices_on_tailor_id      (tailor_id)
#

class Price < ActiveRecord::Base
  belongs_to :tailor
  belongs_to :price_list
  
  register_currency :eur
  monetize :price_consumer_cents, with_currency: :eur
  monetize :price_business_cents, with_currency: :eur
end

 
