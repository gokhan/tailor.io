# == Schema Information
#
# Table name: social_connections
#
#  id         :integer          not null, primary key
#  tailor_id  :integer
#  facebook   :string
#  twitter    :string
#  youtube    :string
#  instagram  :string
#  googleplus :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_social_connections_on_tailor_id  (tailor_id)
#

class SocialConnection < ActiveRecord::Base
  belongs_to :tailor
end
