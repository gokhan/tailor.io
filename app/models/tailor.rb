# == Schema Information
#
# Table name: tailors
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  slug        :string
#  tag_line    :string
#  state       :string           default("pending")
#
# Indexes
#
#  index_tailor_on_slug     (slug) UNIQUE
#  index_tailor_on_user_id  (user_id)
#

class Tailor < ActiveRecord::Base
  belongs_to :user
  has_one :address, as: :addressable, dependent: :destroy
  has_one :seo, dependent: :destroy
  has_one :social_connection, dependent: :destroy
  has_one :opening_hour, dependent: :destroy
  has_many :images,-> { order(position: :asc) }, as: :imageable, dependent: :destroy
  has_many :prices, dependent: :destroy

  scope :approved, -> { where(state: 'approved') }

  extend FriendlyId
  friendly_id :name, use: :slugged

  def avatar
  	images.first || Image.first
  end

  def logo
    imgs = images.tagged_with('logo')
    imgs.any? ? images.first : Image.first
  end
end
