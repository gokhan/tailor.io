# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  imageable_id   :integer
#  imageable_type :string
#  data           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  position       :integer
#
# Indexes
#
#  index_images_on_imageable_type_and_imageable_id  (imageable_type,imageable_id)
#

class Image < ActiveRecord::Base
  mount_uploader :data, ImageUploader
  belongs_to :imageable, polymorphic: true
  acts_as_taggable
  acts_as_list scope: [:imageable_type, :imageable_id]
end
