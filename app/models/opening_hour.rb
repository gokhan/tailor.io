# == Schema Information
#
# Table name: opening_hours
#
#  id         :integer          not null, primary key
#  tailor_id  :integer
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_opening_hours_on_tailor_id  (tailor_id)
#

class OpeningHour < ActiveRecord::Base
  belongs_to :tailor
end
