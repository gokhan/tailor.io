class PagesController < ApplicationController
  def show
    run Page::Show 
    render html: concept(Page::Cell::Show, @model, context: {current_user: current_user}, layout: Site::Cell::Layout)
  end
end
