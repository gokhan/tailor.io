class Dashboard::ImagesController < AdminController
  def create
    run Image::Create do |result|
      return( render( json:  TailorRepresenter.new(result["model"].imageable) ) )
    end
  end

  def logolize
    run Image::Logolize do |result|
      return( render( json:  TailorRepresenter.new(result["model"].imageable) ) )
    end
  end

  def destroy
  	run Image::Destroy 
    render nothing: true, status: :ok
  end
end