class Dashboard::SocialConnectionsController < AdminController
  # create a new address
  def new    
    result = run SocialConnection::Create::Present
    render html: concept(SocialConnection::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def create
    run SocialConnection::Create do |result|
      return redirect_to [ :dashboard, result["model"].tailor ]
    end
    render html: concept(SocialConnection::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  # update current address
  def edit    
 		result = run SocialConnection::Update::Present
    render html: concept(SocialConnection::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def update
  	run SocialConnection::Update do |result|
      return redirect_to [ :dashboard, result["model"].tailor ]
    end
    render html: concept(SocialConnection::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end
end