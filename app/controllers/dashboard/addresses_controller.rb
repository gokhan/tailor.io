class Dashboard::AddressesController < AdminController
  # create a new address
  def new    
    result = run Address::Create::Present
    render html: concept(Address::Cell::Form, @form, context: { current_user: current_user },layout: Site::Cell::Dashboard)
  end

  def create
    run Address::Create do |result|
      return redirect_to [ :dashboard, result["model"].addressable ]
    end
    render html: concept(Address::Cell::Form, @form, context: { current_user: current_user },layout: Site::Cell::Dashboard)
  end

  # update current address
  def edit    
 	  run Address::Update::Present do |result|
      render html: concept(Address::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
    end
  end

  def update
  	run Address::Update do |result|
      return redirect_to [ :dashboard, result["model"].addressable ]
    end
    render html: concept(Address::Cell::Form, @form, context: { current_user: current_user },layout: Site::Cell::Dashboard)
  end
end