class Dashboard::OpeningHoursController < AdminController
  # create a new address
  def new    
    result = run OpeningHour::Create::Present
    render html: concept(OpeningHour::Cell::Edit, @form, context: { current_user: current_user },layout: Site::Cell::Dashboard)
  end

  def create
    run OpeningHour::Create do |result|
      return redirect_to [ :dashboard, result["model"].tailor ]
    end
    render html: concept(OpeningHour::Cell::Edit, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  # update current address
  def edit    
 		run OpeningHour::Update::Present do |result|
      render html: concept(OpeningHour::Cell::Edit, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
    end
  end

  def update
  	run OpeningHour::Update do |result|
      return redirect_to [ :dashboard, result["model"].tailor ]
    end
    render html: concept(OpeningHour::Cell::Edit, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end
end