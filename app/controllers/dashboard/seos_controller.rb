class Dashboard::SeosController < AdminController
  # create a new address
  def new    
    result = run Seo::Create::Present
    render html: concept(Seo::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def create
    run Seo::Create do |result|
      return redirect_to [ :dashboard, result["model"].tailor ]
    end
    render html: concept(Seo::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  # update current address
  def edit    
 		result = run Seo::Update::Present
    render html: concept(Seo::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def update
  	run Seo::Update do |result|
      return redirect_to [ :dashboard, result["model"].tailor ]
    end
    render html: concept(Seo::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end
end