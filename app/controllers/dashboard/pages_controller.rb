class Dashboard::PagesController < AdminController
  def index
    run Page::All do |result|
      return render( html: concept(Page::Cell::Index, result["model"], context: { current_user: current_user }, layout: Site::Cell::Dashboard) )
    end
    return redirect_to '/home/access_denied'
  end

  def new
    run Page::Create::Present do |result|
      return render( html: concept(Page::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard) )
    end
    return redirect_to '/home/access_denied'
  end

  def create
    run Page::Create do |result|
      return redirect_to [ :dashboard, :pages ]
    end
    render html: concept(Page::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end
  
  def edit    
    result = run Page::Update::Present
    render html: concept(Page::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def update
    run Page::Update do |result|
      return redirect_to [ :dashboard, :pages ]
    end
    render html: concept(Page::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def show
    result = run Page::Show
    render html: concept(Page::Cell::Show, result["model"], context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end
end