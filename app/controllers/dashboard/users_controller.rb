class Dashboard::UsersController < AdminController
  def index
    run User::All do |result|
      return render( html: concept(User::Cell::Index, result["model"], context: { current_user: current_user }, layout: Site::Cell::Dashboard) )
    end    
    return redirect_to '/home/access_denied'
  end

  def new
    result = run User::Create::Present
    render html: concept(User::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def create
    run User::Create do |result|
      return redirect_to root_path, notice: 'You are successfully registered, we will contact you to proceed with your application'  
    end
    render html: concept(User::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end
  
  def edit
    run User::Update::Present do |result|
      return render( html: concept(User::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard) )
    end
    return redirect_to '/home/access_denied'
  end

  def create
    run User::Create do |result|
      return redirect_to [ :dashboard, :users ]      
    end
    render html: concept(User::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end  

  def update
    run User::Update do |result|
      return redirect_to [ :dashboard, :users]
    end
    render html: concept(User::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end  
end