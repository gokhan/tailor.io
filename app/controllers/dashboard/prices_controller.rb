class Dashboard::PricesController < AdminController
  def index
    run Price::Index do |result|
      return render html: concept(Price::Cell::Index, result["model"], context: { current_user: current_user }, layout: Site::Cell::Dashboard)
    end
    return redirect_to '/home/access_denied'
  end
  # create a new address
  def new    
    result = run Price::Create::Present
    render html: concept(Price::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def create
    run Price::Create do |result|
      return redirect_to [ :dashboard, result["model"].tailor, :prices ]
    end
    render html: concept(Price::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  # update current address
  def edit    
 		result = run Price::Update::Present
    render html: concept(Price::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def update
  	run Price::Update do |result|
      return redirect_to [ :dashboard, result["model"].tailor, :prices ]
    end
    render html: concept(Price::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def destroy
    run Price::Delete
    return redirect_to [ :dashboard, result["model"].tailor, :prices ]
  end
end