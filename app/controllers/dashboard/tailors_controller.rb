class Dashboard::TailorsController < AdminController
  def index
    run Tailor::All do |result|
      return render html: concept(Tailor::Cell::Index, result["model"], context: { current_user: current_user }, layout: Site::Cell::Dashboard)
    end
    return redirect_to '/home/access_denied'
  end

  def new
    run Tailor::Create::Present do |result|
      return render html: concept(Tailor::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
    end
    return redirect_to '/home/access_denied'
  end

  def create
    run Tailor::Create do |result|
      return redirect_to [ :dashboard, result["model"] ]
    end
    render html: concept(Tailor::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end
  
  def edit    
    run Tailor::Update::Present do |result|
      return render( html: concept(Tailor::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard) )
    end
    return redirect_to '/home/access_denied'
  end

  def update
    run Tailor::Update do |result|
      return redirect_to [ :dashboard, result["model"] ]
    end
    render html: concept(Tailor::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def show
    result = run Tailor::Show
    render html: concept(Tailor::Cell::Show, result["model"], context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def change_owner_form
    run Tailor::ChangeOwner::Present 
    render html: concept(Tailor::Cell::ChangeOwner, @form, context: { current_user: current_user }, layout: Site::Cell::Dashboard)
  end

  def change_owner
    run Tailor::ChangeOwner
    redirect_to [ :dashboard, :tailors ]
  end

end