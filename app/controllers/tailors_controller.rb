class TailorsController < ApplicationController
  def show
    run Tailor::Show 
    render html: concept(Tailor::Cell::Site, @model, context: {current_user: current_user}, layout: Site::Cell::Layout)
  end

 	def prices
    run Tailor::Show 
    render html: concept(Tailor::Cell::Prices, @model, context: {current_user: current_user}, layout: Site::Cell::Layout)
 	end
end
