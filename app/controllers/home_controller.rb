class HomeController < ApplicationController
  def index
    run Tailor::Featured

    render html: concept( Site::Cell::Index, result["model"],  
    											context: { current_user: current_user }, 
    											layout: Site::Cell::Layout)
  end

  def search
    run Tailor::Search do |result|
      render html: concept( Site::Cell::Map, result["model"],  
                            context: { current_user: current_user }, 
                            layout: Site::Cell::Layout)
    end
  end

  def access_denied
    render html: concept( Site::Cell::AccessDenied, nil,
    											context: { current_user: current_user }, 
    											layout: Site::Cell::Layout)
  end
end
