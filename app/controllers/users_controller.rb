class UsersController < ApplicationController
  def new
#    @user = User.new
    result = run User::Create::Present
    render html: concept(User::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Layout)
  end


  def create
    run User::Create do |result|
      return redirect_to [ :dashboard, :users ]      
    end
    
    render html: concept(User::Cell::Form, @form, context: { current_user: current_user }, layout: Site::Cell::Layout)
  end



  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end