require 'carrierwave/processing/mini_magick' 
class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick


  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Process files as they are uploaded:
  process resize_to_fit: [1400, 1400]

  # Create different versions of your uploaded files:
  version :normal do
    process resize_to_fill: [1000, 750]
  end

  version :pad do
    process resize_and_pad: [1000, 750]
  end

  # Create different versions of your uploaded files:
  version :thumb do
    process resize_to_fit: [150, 150]
  end

  version :square do
    process resize_to_fill: [300, 300]
  end

  # Create different versions of your uploaded files:
  version :medium do
    process resize_to_fit: [600, 600]
  end

  version :medium_square do
    process resize_to_fill: [600, 600]
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_whitelist
    %w(jpg jpeg gif png)
  end


end
