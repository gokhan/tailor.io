require 'csv'

filename = 'db/data/tailors.csv'
i=0
CSV.foreach(filename, col_sep: ',', row_sep: :auto, headers: true) do |row|
  tailor = Tailor.create(name: row[0], description: row[0])
  # 1           2       3                 4         5           6         7         8          9        10
  # Straatnaam  Huisnr  Toevoeging_huisnr Postcode  Woonplaats  Provincie Landcode  Tel_Netnr  Tel_Abnr Domeinnaam
  house_number_with_extension = [row[2], row[3]].reject(&:blank?).join(" ")
  phone_number = [row[8], row[9]].reject(&:blank?).join(" ")
  tailor.create_address(
    address: "#{row[1]} #{house_number_with_extension} #{row[4]} #{row[5]} #{row[6]}",
    house_number: house_number_with_extension,
    street: row[1],
    city: row[5],
    state: row[6],
    postcode: row[4],
    country: 'Netherlands',
    phone: phone_number,
    uri: row[10],
  )
  i += 1
  raise 'Stop' if i > 10
end

#  id               :integer          not null, primary key
#  address          :string
#  addressable_id   :integer
#  addressable_type :string
#  house_number     :string
#  street           :string
#  city             :string
#  postcode         :string
#  state            :string
#  country          :string
#  lat              :decimal(10, 6)
#  lng              :decimal(10, 6)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  phone            :string
#  email            :string
