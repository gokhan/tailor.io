# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create([
    {name: 'Gokhan ARLI', email: 'gokhan@sylow.net', password: '@Gokhan007', admin: true},
    {name: 'Yavuz Gedik', email: 'yavuz.gedik01@gmail.com', password: '@Gokhan007', admin: true},
  ])  

# Tailor.create([
#     { 
#       user: User.find_by(email: 'gokhan@sylow.net'),
#       name: 'Sylow Web Development', description: 'We develop websites on time and help you to understand your business specs', 
#       house_number: '90', street: 'Arendsweg', city: 'Alkmaar', state: 'NH', postcode: '1826JE', lat: '52.6603039', lng: '4.768324799999959'
#     }
#   ])  

# tailor = Shop.first

# 1.upto(3) do |i|
#   tailor.images.create(data: Rails.root.join("app/assets/images/samples/#{i}.jpg").open)
# end

PriceCategory.create([
  {name: 'Pantalon', key:'pantalon'},
  {name: 'Spijkerbroek', key:'spijkerbroek'},
  {name: 'Rok', key:'rok'},
  {name: 'Overhemd / Blouse', key:'overhemd_blouse'},
  {name: 'Colbert / Mantel / Jas', key:'colbert'},
  {name: 'Leer / Suéde Pantalon', key:'leer_pantalon'},
  {name: 'Leer / Suéde Rok', key:'leer_rok'},
  {name: 'Leer / Suéde Jas', key:'leer_jas'},
  {name: 'Kleding', key:'dry_cleaning_kleding', dry_cleaning: true},
  {name: 'Beddengoed', key:'dry_cleaning_beddengoed', dry_cleaning: true},
  {name: 'Gordijnen & Tapijten', key:'dry_cleaning_gordijnen_tapijten', dry_cleaning: true},
  {name: 'Hoezen', key:'dry_cleaning_hoezen', dry_cleaning: true},
  {name: 'Suede & Leer', key:'dry_cleaning_suede_leer', dry_cleaning: true},
  {name: 'wassen', key:'dry_cleaning_wassen', dry_cleaning: true},
  ])


PriceCategory.where(dry_cleaning: true).all.each do |category|
  CSV.foreach("#{Rails.root}/db/csv/#{category.key}") do |row|
    PriceList.create!(name: row[0], price_category: category)
  end
end

