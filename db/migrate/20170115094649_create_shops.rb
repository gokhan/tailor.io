class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.text :description
      t.string :house_number
      t.string :street
      t.string :city
      t.string :postcode
      t.string :state
      t.string :country
      t.decimal :lat, {precision: 10, scale: 6}
      t.decimal :lng, {precision: 10, scale: 6}

      t.timestamps null: false
    end
  end
end
