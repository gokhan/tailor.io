class CreateSocialConnections < ActiveRecord::Migration
  def change
    create_table :social_connections do |t|
      t.references :shop, index: true
      t.string :facebook
      t.string :twitter
      t.string :youtube
      t.string :instagram
      t.string :googleplus


      t.timestamps null: false
    end
    add_foreign_key :social_connections, :shops
  end
end
