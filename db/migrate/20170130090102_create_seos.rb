class CreateSeos < ActiveRecord::Migration
  def change
    create_table :seos do |t|
      t.references :shop, index: true
      t.string :title
      t.string :description
      t.string :keywords

      t.timestamps null: false
    end
    add_foreign_key :seos, :shops
  end
end
