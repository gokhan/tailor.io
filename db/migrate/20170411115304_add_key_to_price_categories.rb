class AddKeyToPriceCategories < ActiveRecord::Migration
  def change
    add_column :price_categories, :key, :string
  end
end
