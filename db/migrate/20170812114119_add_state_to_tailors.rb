class AddStateToTailors < ActiveRecord::Migration
  def change
    add_column :tailors, :state, :string, default: 'pending'
    Tailor.update_all(state: 'approved')
  end
end
