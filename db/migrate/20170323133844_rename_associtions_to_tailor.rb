class RenameAssocitionsToTailor < ActiveRecord::Migration
  def change
  	rename_column :opening_hours, :shop_id, :tailor_id
  	rename_column :prices, :shop_id, :tailor_id
  	rename_column :seos, :shop_id, :tailor_id
  	rename_column :social_connections, :shop_id, :tailor_id
  end
end
