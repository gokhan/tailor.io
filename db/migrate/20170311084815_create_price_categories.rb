class CreatePriceCategories < ActiveRecord::Migration
  def change
    create_table :price_categories do |t|
      t.string :name

      t.timestamps null: false
    end

     add_reference :prices, :price_category, index: true
  end
end
