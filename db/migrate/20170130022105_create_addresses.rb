class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :address
      t.references :addressable, polymorphic: true, index: true
      t.string :house_number
      t.string :street
      t.string :city
      t.string :postcode
      t.string :city
      t.string :state
      t.string :country
      t.decimal :lat, {precision: 10, scale: 6}
      t.decimal :lng, {precision: 10, scale: 6}

      t.timestamps null: false
    end

    remove_columns :shops, :address,  :house_number, :street, :city, :postcode, :state, :country, :lat, :lng
  end
end
