class AddDryCleaningToPriceCategories < ActiveRecord::Migration
  def change
    add_column :price_categories, :dry_cleaning, :boolean, default: false
  end
end
