class CreatePriceLists < ActiveRecord::Migration
  def change
    create_table :price_lists do |t|
      t.string :name
      t.references :price_category, index: true

      t.timestamps null: false
    end
    add_foreign_key :price_lists, :price_categories
  end
end
