class AddUriToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :uri, :string
  end
end
