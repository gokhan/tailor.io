class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.references :shop, index: true
      t.string :text
      t.integer :price_cents

      t.timestamps null: false
    end
    add_foreign_key :prices, :shops
  end
end
