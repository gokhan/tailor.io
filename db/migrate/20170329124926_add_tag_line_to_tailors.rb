class AddTagLineToTailors < ActiveRecord::Migration
  def change
    add_column :tailors, :tag_line, :string
  end
end
