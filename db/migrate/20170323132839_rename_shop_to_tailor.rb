class RenameShopToTailor < ActiveRecord::Migration
  def change
  	 rename_table :shops, :tailors
  end
end
