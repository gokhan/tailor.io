class CreateOpeningHours < ActiveRecord::Migration
  def change
    create_table :opening_hours do |t|
      t.references :shop, index: true
      t.text :content

      t.timestamps null: false
    end
    add_foreign_key :opening_hours, :shops
  end
end
