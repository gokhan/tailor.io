class RestructurePrices < ActiveRecord::Migration
  def change
  	rename_column :prices, :price_cents, :price_consumer_cents
  	add_column :prices, :price_business_cents, :integer
    
    add_reference :prices, :price_list, index: true
    add_foreign_key :prices, :price_lists
    
    remove_column :prices, :price_category_id
    remove_column :prices, :text
  end
end
