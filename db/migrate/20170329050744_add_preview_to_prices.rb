class AddPreviewToPrices < ActiveRecord::Migration
  def change
    add_column :prices, :preview, :boolean, default: false
  end
end
