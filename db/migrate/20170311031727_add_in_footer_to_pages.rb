class AddInFooterToPages < ActiveRecord::Migration
  def change
    add_column :pages, :in_footer, :boolean, default: false
  end
end
