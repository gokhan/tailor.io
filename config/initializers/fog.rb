require 'carrierwave/storage/abstract'
require 'carrierwave/storage/file'
require 'carrierwave/storage/fog'

if Rails.env.production?
  CarrierWave.configure do |config|
    config.storage = :fog  
    config.fog_credentials = {
      provider:              'AWS',                       
      aws_access_key_id:     ENV['AWS_ACCESS_KEY_ID'],    
      aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],       
      region:                ENV['AWS_REGION'],                  
    }
    config.fog_directory  = ENV['AWS_NAME_OF_BUCKET']                    
    config.fog_public     = false
    config.fog_authenticated_url_expiration = 600
    config.fog_attributes = { 'x-amz-server-side-encryption' => 'AES256','Cache-Control' => "max-age=#{365.day.to_i}" }  
  end
else
  CarrierWave.configure do |config|
    config.storage = :file
  end
end
