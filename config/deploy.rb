require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/rbenv'
require 'mina_sidekiq/tasks'
require 'mina/unicorn'

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :domain, '146.185.169.31'
set :deploy_to, '/home/deployer/tailorrepair'
set :repository, 'git@bitbucket.org:gokhan/tailor.io.git'
set :branch, 'master'
set :user, 'deployer'
set :forward_agent, true
set :port, '22'
set :unicorn_pid, "#{fetch(:deploy_to)}/shared/pids/unicorn.pid"
set :rails_env, 'production'

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['log', 'tmp/sockets', 'tmp/pids']
set :shared_files, ['config/database.yml', 'config/secrets.yml', '.env']


# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  command %{
echo "-----> Loading environment"
#{echo_cmd %[source ~/.bashrc]}
}
  invoke :'rbenv:load'
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .rbenv-version to your repository.
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do
  command %[mkdir -p "#{fetch(:deploy_to)}/shared/sockets"]
  command %[chmod g+rx,u+rwx "#{fetch(:deploy_to)}/shared/sockets"]

  command %[mkdir -p "#{fetch(:deploy_to)}/shared/log"]
  command %[chmod g+rx,u+rwx "#{fetch(:deploy_to)}/shared/log"]

  command %[mkdir -p "#{fetch(:deploy_to)}/shared/config"]
  command %[chmod g+rx,u+rwx "#{fetch(:deploy_to)}/shared/config"]

  command %[touch "#{fetch(:deploy_to)}/shared/config/database.yml"]
  command  %[echo "-----> Be sure to edit 'shared/config/database.yml'."]

  command %[touch "#{fetch(:deploy_to)}/shared/config/secrets.yml"]
  command %[echo "-----> Be sure to edit 'shared/config/secrets.yml'."]

  command %[touch "#{fetch(:deploy_to)}/shared/config/env.yml"]
  command %[echo "-----> Be sure to edit 'shared/config/env.yml'."]

  # sidekiq needs a place to store its pid file and log file
  command %[mkdir -p "#{fetch(:deploy_to)}/shared/pids/"]
  command %[chmod g+rx,u+rwx "#{fetch(:deploy_to)}/shared/pids"]

end


desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do

    # stop accepting new workers
    invoke :'sidekiq:quiet'

    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'

    on :launch => :environment do
      invoke :'sidekiq:restart'
      invoke :'unicorn:restart'
    end
  end
end